import { Platform, StatusBar, View, Dimensions } from 'react-native'
import React from 'react'

const {height} = Dimensions.get('screen')

export default function GlobalStatusBar({ barStyle, backgroundColor, translucent }) {
  return (
    <View style={Platform.OS === "android" && { marginBottom: height /20}}>
      <StatusBar
        translucent={translucent}
        barStyle={barStyle}
        backgroundColor={backgroundColor}
      />
    </View>
  );
}