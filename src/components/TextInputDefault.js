import {StyleSheet, TextInput, View} from 'react-native';
import React from 'react';
import Color from '../configs/Color';

export default function TextInputDefault({
  placeholder,
  value,
  onChangeText,
  style,
  secureTextEntry,
}) {
  return (
    <View>
      <TextInput
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
        placeholderTextColor={Color.textGrey}
        style={[styles.textInputStyle, style]}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  textInputStyle: {
    backgroundColor: Color.lineGrey,
    width: '100%',
    borderRadius: 8,
    padding: 13,
  },
});
