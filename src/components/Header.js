import { StyleSheet, TouchableOpacity, View } from "react-native";
import React from "react";
import ChevronLeft from "../assets/svg/ChevronLeft";
import TextDefault from "./TextDefault";
import Color from "../configs/Color";
import Styles from "../configs/Styles";
import { useNavigation } from "@react-navigation/native";

export default function Header({title}) {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={[Styles.flexDirectRow, Styles.marginBottomSmall]}>
        <TouchableOpacity style={styles.chevronLeftIcon} onPress={() => navigation.goBack()}>
          <ChevronLeft />
        </TouchableOpacity>
        <View style={styles.title}>
          <TextDefault text={title} h1Size />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderBottomColor: Color.lineGrey,
    borderBottomWidth: 3,
    paddingHorizontal: 10,
  },
  title: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    position: "absolute",
    zIndex: 0,
  },
  chevronLeftIcon:{
    zIndex: 100
  }
});
