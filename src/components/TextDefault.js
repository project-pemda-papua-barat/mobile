import {Text, View} from 'react-native';
import React from 'react';

export default function TextDefault({
  text,
  bold,
  color,
  whiteColor,
  style,
  titleSize,
  h1Size,
  fontWeight,
  textStyle,
  numberOfLines,
}) {
  return (
    <View style={style}>
      <Text
        numberOfLines={numberOfLines}
        style={[
          {
            fontWeight: bold ? "bold" : fontWeight ? fontWeight : "500",
            color: color ? color : whiteColor ? "white" : "black",
            fontSize: titleSize ? 14 : h1Size ? 18 : 12,
          },
          textStyle,
        ]}
      >
        {text}
      </Text>
    </View>
  );
}
