import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import TextDefault from './TextDefault';
import Color from '../configs/Color';

export default function ButtonDefault({
  buttonTitle,
  onPress,
  buttonStyle,
  withoutBg,
  textStyle,
  textColor,
  backgroundError,
  backgroundWhite,
  isBold
}) {
  return (
    <View>
      {withoutBg ? (
        <TouchableOpacity
          style={[withoutBg ? null : styles.container, buttonStyle]}
          onPress={onPress}
        >
          <TextDefault
            text={buttonTitle}
            color={textColor}
            bold={isBold}
            style={textStyle}
          />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={[
            buttonStyle ? buttonStyle : styles.container,
            {
              backgroundColor: backgroundError
                ? Color.error
                : backgroundWhite
                ? Color.backgroundWhite
                : Color.primary,
            },
          ]}
          onPress={onPress}
        >
          <TextDefault
            text={buttonTitle}
            bold
            color={textColor ? textColor : Color.backgroundWhite}
          />
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 13,
    borderRadius: 8,
    alignItems: 'center',
  },
});
