import React from "react";
import { StyleSheet, ActivityIndicator, View } from "react-native";
import Modal from "react-native-modal";
import Color from "../configs/Color";

export default function Loading(props) {
  return (
    <Modal
      animationIn="fadeIn"
      animationOut="fadeOut"
      animationInTiming={0}
      backdropTransitionInTiming={0}
      backdropTransitionOutTiming={0}
      backdropColor="black"
      style={styles.loadingWrapper}
      isVisible={props.isVisible}
    >
      <View style={styles.backGround}>
        <ActivityIndicator
          size="large"
          color={Color.primary}
        />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  loadingWrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  backGround: {
    backgroundColor: Color.backgroundWhite,
    padding: 30,
    borderRadius: 10,
  },
});
