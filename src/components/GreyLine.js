import {StyleSheet, View} from 'react-native';
import React from 'react';
import Color from '../configs/Color';

export default function GreyLine() {
  return <View style={styles.container} />;
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.lineGrey,
    width: '100%',
    height: 10,
  },
});
