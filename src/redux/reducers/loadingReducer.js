import { LOADING_VISIBLE } from "../types";

const initialState = {
  loading: false,
};

const loadingReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOADING_VISIBLE:
      return {
        ...state,
        loading: payload,
      };
    default:
      return state;
  }
};

export default loadingReducer;
