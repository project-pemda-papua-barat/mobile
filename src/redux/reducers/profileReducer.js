import {
  GET_PROFILE_DATA, LOADING_AUTH_VISIBLE, RESPONSE_ERROR
} from "../types";

const initialState = {
  data: {},
  error: '',
  loadingAuth : false
};

const profileReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_PROFILE_DATA:
      return {
        ...state,
        data: payload,
      };
    case RESPONSE_ERROR:
      return {
        ...state,
        error: payload,
      };
    case LOADING_AUTH_VISIBLE:
      return {
        ...state,
        loadingAuth: payload,
      };
    default:
      return state;
  }
};

export default profileReducer;
