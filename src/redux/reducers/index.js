import { combineReducers } from "redux";
import authReducer from "./authReducer";
import loadingReducer from "./loadingReducer";
import profileReducer from "./profileReducer"

export default combineReducers({
  authReducer,
  loadingReducer,
  profileReducer,
});
