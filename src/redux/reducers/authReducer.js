import { DELETE_RESPONSE_AUTH, LOADING_AUTH_VISIBLE, RESPONSE_AUTH_DATA, RESPONSE_ERROR } from "../types";

const initialState = {
  error: '',
  response: '',
  loading: false
};

const authReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case RESPONSE_ERROR:
      return {
        ...state,
        error: payload,
      };
    case LOADING_AUTH_VISIBLE:
      return {
        ...state,
        loading: payload,
      };
    default:
      return state;
  }
};

export default authReducer;
