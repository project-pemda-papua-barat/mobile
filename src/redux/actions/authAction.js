import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { AuthUrl } from "../../configs/APIs";
import {
  RESPONSE_ERROR,
} from "../types";
import { setLoadingBarVisible, setLoadingLoginBarVisible } from "./loadingAction";

export const postUserData = (email, password) => (dispatch) =>
  new Promise((resolve) => {
    dispatch(setLoadingLoginBarVisible(true)).then(() => {
      var data = JSON.stringify({
        username: email,
        password: password,
      });
      var config = {
        method: "post",
        url: AuthUrl,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data: data,
      };

      axios(config)
        .then(function (response) {
          if (__DEV__) console.log(response.data);
          AsyncStorage.setItem("token", response.data.token, (err) => {
            dispatch(setLoadingLoginBarVisible(false)).then(()=> {
              dispatch({
                type: RESPONSE_ERROR,
                payload: '',
              });
            })
          });
          resolve(response.data.status);
        })
        .catch(function ({ response }) {
          dispatch(setLoadingLoginBarVisible(false)).then(() => {
            dispatch({
              type: RESPONSE_ERROR,
              payload: response.data.message,
            });
            resolve(response.data.message);
          });
        });
    });
  });

export const deleteUserData = () => (dispatch) =>
  new Promise((resolve) => {
      AsyncStorage.removeItem("token", (err) => {
        resolve()
      })
  });
