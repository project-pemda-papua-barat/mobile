import { LOADING_AUTH_VISIBLE, LOADING_VISIBLE } from "../types";

export const setLoadingBarVisible = (isLoading) => (dispatch) => new Promise((resolve) => {
    dispatch({
        type: LOADING_VISIBLE,
        payload: isLoading
    });
    resolve();
});

export const setLoadingLoginBarVisible = (isLoading) => (dispatch) =>
  new Promise((resolve) => {
    dispatch({
      type: LOADING_AUTH_VISIBLE,
      payload: isLoading,
    });
    resolve();
  });