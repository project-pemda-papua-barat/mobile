import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { getProfileUrl } from "../../configs/APIs";
import { GET_PROFILE_DATA, RESPONSE_ERROR } from "../types";
import { setLoadingBarVisible } from "./loadingAction";


export const getUserProfile = () => (dispatch) =>
  new Promise((resolve) => {
    dispatch(setLoadingBarVisible(true)).then(() => {
        AsyncStorage.getItem('token', (err, token) => {
            var config = {
              method: "get",
              url: getProfileUrl,
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: `bearer ${token}`,
              },
            };
      
            axios(config)
              .then(function (response) {
                dispatch(setLoadingBarVisible(false)).then(()=> {
                  dispatch({
                    type: GET_PROFILE_DATA,
                    payload: response.data,
                  });
                })
                resolve(response.data)
              })
              .catch(function (error) {
                dispatch(setLoadingBarVisible(false)).then(() => {
                  dispatch({
                      type: RESPONSE_ERROR,
                      payload: error.message
                  })
                });
              });
        })
        .catch(()=> dispatch(setLoadingBarVisible(false)))
    });
  });
