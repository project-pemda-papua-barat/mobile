export const BaseUrl = "https://papua-barat-dev.hadyanmerchant.id";

export const AuthUrl = `${BaseUrl}/service/auth`
export const getProfileUrl = `${BaseUrl}/service/profile`;
