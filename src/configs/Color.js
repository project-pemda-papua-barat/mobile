export default {
  primary: "#00a0e3",
  secondary: "#b0cb1f",
  backgroundWhite: "#FFF",
  error: "#FF4747",
  lineGrey: "#F4F4F4",
  dotsGrey: "#CACACA",
  black: "#AAAAA",
  textGrey: "#858585",
  softLineGrey: "#F5F4F2",
};
