import {ImageBackground, StyleSheet, View, Dimensions, Image, FlatList, TouchableOpacity, BackHandler, ActivityIndicator} from 'react-native';
import React, { useEffect } from 'react';
import Styles from '../../configs/Styles';
import GlobalStatusBar from '../../components/GlobalStatusBar';
import Pengumuman from '../authScreen/Pengumuman';
import Color from '../../configs/Color';
import TextDefault from '../../components/TextDefault';
import Bell from '../../assets/svg/Bell'
import { useDispatch, useSelector } from 'react-redux';
import { getUserProfile } from '../../redux/actions/profileAction';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { SKELETON_BG, SKELETON_HIGHLIGHT, SKELETON_SPEED } from '../../configs/config';

const {height} = Dimensions.get('screen')

export default function HomeScreen(props) {
  const dispatch = useDispatch()
  const { data } = useSelector((state) => state.profileReducer);
  const { loading } = useSelector((state) => state.loadingReducer);


  useEffect(()=> {
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      () => true
    );
    return () => {
      backHandler.remove()
    }
  },[])

  useEffect(() => {
    dispatch(getUserProfile());
  }, []);

  const homeMenu = [
    {
      id: 1,
      name: "Scan QRcode",
      icon: require("../../assets/png/barcode.png"),
    },
    {
      id: 2,
      name: "Transaksi",
      icon: require("../../assets/png/transaksi.png"),
    },
    {
      id: 3,
      name: "Grafik",
      icon: require("../../assets/png/grafik.png"),
    },
    {
      id: 4,
      name: "Laporan",
      icon: require("../../assets/png/laporan.png"),
    },
    {
      id: 5,
      name: "Aset",
      icon: require("../../assets/png/aset.png"),
    },
    {
      id: 6,
      name: "Sistem",
      icon: require("../../assets/png/sistem.png"),
    },
    {
      id: 7,
      name: "Master Data",
      icon: require("../../assets/png/masterdata.png"),
    },
  ];

  const menuHandle = (item) => {
    alert("Fitur " + item + " Sedang di Develop")
  }

  return (
    <ImageBackground
      style={Styles.imageBgContainer}
      source={require("../../assets/png/BG3x.png")}
    >
      <ImageBackground
        style={[styles.homeGradient, Styles.paddingHorizontal]}
        source={require("../../assets/png/home_gradient.png")}
      >
        <GlobalStatusBar
          barStyle={"light-content"}
          backgroundColor={"transparent"}
          translucent
        />
      </ImageBackground>
      <View style={styles.absoluteView}>
        <View style={styles.pengumumanHome}>
          <View
            style={[
              Styles.flexDirectRowBetween,
              Styles.paddingHorizontal,
              Styles.marginBottomDefault,
            ]}
          >
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate("ProfileScreen");
                dispatch(getUserProfile());
              }}
              style={Styles.flexDirectRow}
            >
              {loading ? (
                <View style={[styles.border, Styles.marginRightSmall]}>
                  <ActivityIndicator color={Color.black} />
                </View>
              ) : (
                <Image
                  style={[Styles.marginRightSmall, styles.border]}
                  source={{ uri: data.profile_pict }}
                />
              )}
              <TextDefault text={"Hi, "} whiteColor bold />
              {loading ? (
                <SkeletonPlaceholder
                  speed={SKELETON_SPEED}
                  backgroundColor={SKELETON_BG}
                  highlightColor={SKELETON_HIGHLIGHT}
                >
                  <SkeletonPlaceholder.Item
                    width={80}
                    height={10}
                    borderRadius={2}
                  />
                </SkeletonPlaceholder>
              ) : (
                <TextDefault text={data.username} whiteColor bold />
              )}
            </TouchableOpacity>
            <TouchableOpacity>
              <Bell />
            </TouchableOpacity>
          </View>
          <Pengumuman backgroundColor={Color.backgroundWhite} />
        </View>
        <View>
          <FlatList
            data={homeMenu}
            keyExtractor={(item) => item.id}
            numColumns={3}
            columnWrapperStyle={[
              styles.columnWrapperStyle,
              Styles.paddingHorizontal,
              Styles.marginBottomDefault,
            ]}
            renderItem={({ item, index }) => {
              return (
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => menuHandle(item.name)}
                  style={styles.renderItemWrapper}
                >
                  <View style={[styles.iconWrapper, Styles.marginBottomSmall]}>
                    <Image source={item.icon} />
                  </View>
                  <TextDefault fontWeight={"400"} text={item.name} />
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  homeGradient: {
    width: "100%",
    height: height / 4.5,
  },
  absoluteView: {
    position: "absolute",
    top: "8%",
  },
  columnWrapperStyle: {
    justifyContent: "space-between",
    alignItems: "center",
  },
  renderItemWrapper: {
    alignItems: "center",
  },
  border: {
    borderWidth: 1,
    borderColor: Color.backgroundWhite,
    borderRadius: 25,
    width: 32,
    height: 32,
    backgroundColor: Color.backgroundWhite,
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconWrapper: {
    backgroundColor: Color.backgroundWhite,
    width: 96,
    height: 96,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    borderWidth: 1,
    borderColor: Color.lineGrey,
  }
});
