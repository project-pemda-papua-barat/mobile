import {
  Alert,
  BackHandler,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
} from "react-native";
import React, { useEffect } from "react";
import GlobalStatusBar from "../../components/GlobalStatusBar";
import Header from "../../components/Header";
import Styles from "../../configs/Styles";
import TextDefault from "../../components/TextDefault";
import ButtonDefault from "../../components/ButtonDefault";
import { useDispatch, useSelector } from "react-redux";
import { deleteUserData } from "../../redux/actions/authAction";
import Color from "../../configs/Color";
import { getUserProfile } from "../../redux/actions/profileAction";
import GreyLine from "../../components/GreyLine";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Loading from "../../components/Loading";
import { SKELETON_BG, SKELETON_HIGHLIGHT, SKELETON_SPEED } from "../../configs/config";

const { width, height } = Dimensions.get("screen");

export default function ProfileScreen(props) {
  const { data, loadingAuth } = useSelector((state) => state.profileReducer);
  const { loading } = useSelector((state) => state.loadingReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserProfile());
  }, []);

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        handleBackButtonClick
      );
    };
  }, []);

  function handleBackButtonClick() {
    props.navigation.goBack();
    return true;
  }

  const signOutHandler = () => {
    Alert.alert(
      "Sign Out",
      "Anda akan keluar dari akun ini, untuk kembali masuk anda bisa melakukan sign in kembali pada page awal.",
      [
        {
          text: "Kembali",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "Saya ingin keluar",
          onPress: () => confirmSignOut(),
          style: "destructive",
        },
      ]
    );
  };

  const confirmSignOut = () => {
    dispatch(deleteUserData()).then(() => {
      props.navigation.replace("LoginScreen");
    });
  };

  return (
    <SafeAreaView style={Styles.container}>
      <GlobalStatusBar
        barStyle={"dark-content"}
        backgroundColor={"transparent"}
        translucent
      />
      <Header title={"Profile"} />
      <ImageBackground
        style={styles.profileHeader}
        source={require("../../assets/png/home_gradient.png")}
      >
        <View style={styles.profileWrapper}>
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={88}
                height={88}
                borderRadius={50}
              />
            </SkeletonPlaceholder>
          ) : (
            <Image
              source={{ uri: data.profile_pict }}
              style={styles.profilePict}
            />
          )}
        </View>
        <TextDefault text={data.name} whiteColor h1Size bold />
        <View style={[Styles.flexDirectRow, styles.headerButton]}>
          <ButtonDefault
            withoutBg
            buttonStyle={styles.buttonProfile}
            textColor={Color.primary}
            buttonTitle={"Upload Foto"}
          />
          <ButtonDefault
            withoutBg
            buttonStyle={styles.buttonProfile}
            textColor={Color.primary}
            buttonTitle={"Edit Profile"}
          />
        </View>
      </ImageBackground>
      <GreyLine />
      <ScrollView style={styles.profileContainer}>
        <View style={styles.rowProfile}>
          <TextDefault style={styles.textTitle} bold text={"Username"} />
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={200}
                height={15}
                borderRadius={8}
              />
            </SkeletonPlaceholder>
          ) : (
            <TextDefault
              numberOfLines={3}
              textStyle={styles.textRender}
              color={Color.textGrey}
              text={data.username}
            />
          )}
        </View>
        <View style={styles.rowProfile}>
          <TextDefault style={styles.textTitle} bold text={"Email"} />
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={200}
                height={15}
                borderRadius={8}
              />
            </SkeletonPlaceholder>
          ) : (
            <TextDefault
              numberOfLines={3}
              textStyle={styles.textRender}
              color={Color.textGrey}
              text={data.email}
            />
          )}
        </View>
        <View style={styles.rowProfile}>
          <TextDefault style={styles.textTitle} bold text={"Role"} />
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={200}
                height={15}
                borderRadius={8}
              />
            </SkeletonPlaceholder>
          ) : (
            <TextDefault
              numberOfLines={3}
              textStyle={styles.textRender}
              color={Color.textGrey}
              text={data.role_name}
            />
          )}
        </View>
        <View style={styles.rowProfile}>
          <TextDefault style={styles.textTitle} bold text={"Alamat User"} />
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={200}
                height={15}
                borderRadius={8}
              />
            </SkeletonPlaceholder>
          ) : (
            <TextDefault
              numberOfLines={3}
              textStyle={styles.textRender}
              color={Color.textGrey}
              text={data.address_user}
            />
          )}
        </View>
        <View style={styles.rowProfile}>
          <TextDefault style={styles.textTitle} bold text={"No. Identitas"} />
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={200}
                height={15}
                borderRadius={8}
              />
            </SkeletonPlaceholder>
          ) : (
            <TextDefault
              numberOfLines={3}
              textStyle={styles.textRender}
              color={Color.textGrey}
              text={data.squad_phone}
            />
          )}
        </View>
        <View style={styles.rowProfile}>
          <TextDefault
            style={styles.textTitle}
            bold
            text={"No. Telepon Squad"}
          />
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={200}
                height={15}
                borderRadius={8}
              />
            </SkeletonPlaceholder>
          ) : (
            <TextDefault
              numberOfLines={3}
              textStyle={styles.textRender}
              color={Color.textGrey}
              text={data.squad_phone}
            />
          )}
        </View>
        <View style={styles.rowProfile}>
          <TextDefault style={styles.textTitle} bold text={"Unit Squad"} />
          {loading ? (
            <SkeletonPlaceholder>
              <SkeletonPlaceholder.Item
                width={200}
                height={15}
                borderRadius={8}
              />
            </SkeletonPlaceholder>
          ) : (
            <TextDefault
              numberOfLines={3}
              textStyle={styles.textRender}
              color={Color.textGrey}
              text={data.squad_unit}
            />
          )}
        </View>
        <View style={styles.paddingScroll} />
      </ScrollView>
      <Loading isVisible={loadingAuth} />
      <View style={[styles.buttonSignOut, Styles.paddingHorizontal]}>
        <ButtonDefault
          buttonStyle={styles.buttonStyles}
          backgroundError
          buttonTitle={"Sign Out"}
          onPress={signOutHandler}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  profileHeader: {
    width: width,
    height: height / 4,
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  buttonSignOut: {
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? 30 : 0,
    width: width
  },
  buttonStyles: {
    padding: 16,
    alignItems: "center",
    borderRadius: 8,
  },
  headerButton: {
    width: width / 1.6,
    justifyContent: "space-between",
  },
  profilePict: {
    width: 88,
    height: 88,
    borderRadius: 50,
    backgroundColor: Color.backgroundWhite
  },
  profileWrapper: {
    borderWidth: 1,
    borderColor: Color.backgroundWhite,
    borderRadius: 50,
  },
  buttonProfile: {
    backgroundColor: Color.backgroundWhite,
    paddingHorizontal: 25,
    paddingVertical: 8,
    borderRadius: 5,
  },
  rowProfile: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 20,
    borderBottomColor: Color.lineGrey,
    borderBottomWidth: 1,
    width: width,
    paddingHorizontal: 20,
  },
  textTitle: {
    width: width / 3,
    justifyContent: "center",
  },
  textRender: {
    width: width / 1.8,
    textAlign: "right",
    justifyContent: "center",
  },
  profileContainer: {
    flex: 1,
  },
  paddingScroll:{
    height: height / 7
  }
});
