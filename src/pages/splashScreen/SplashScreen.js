import { Image, StyleSheet, View } from 'react-native'
import React, { useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';
import GlobalStatusBar from '../../components/GlobalStatusBar';
import Color from '../../configs/Color';
import { version } from '../../../package.json'
import TextDefault from '../../components/TextDefault'
import { getUserProfile } from '../../redux/actions/profileAction';
import { useDispatch } from 'react-redux';

export default function SplashScreen(props) {
  const dispatch = useDispatch()
  
  useEffect(() => {
    setTimeout(()=>{
      AsyncStorage.getItem("token", (err, res) => {
        if (res) {
          dispatch(getUserProfile());
          props.navigation.navigate("HomeScreen");
        }else{
          props.navigation.navigate("LoginScreen")
        }
      });
    },2000)
  }, []);

  return (
    <View style={styles.container}>
      <GlobalStatusBar
        barStyle={"light-content"}
        translucent
        backgroundColor={"transparent"}
      />
      <Image resizeMode='contain' style={styles.logo} source={require("../../assets/png/pb.png")} />
      <TextDefault
        titleSize={true}
        whiteColor={true}
        text={"Version " + version}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: Color.primary,
  },
  logo:{
    width: 150,
    height:150,
  }
});