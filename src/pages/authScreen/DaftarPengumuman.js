import { BackHandler, FlatList, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import Header from '../../components/Header'
import GlobalStatusBar from '../../components/GlobalStatusBar'
import Styles from '../../configs/Styles'
import Color from '../../configs/Color'
import TextDefault from '../../components/TextDefault'
import ButtonDefault from '../../components/ButtonDefault'

export default function DaftarPengumuman(props) {

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        handleBackButtonClick
      );
    };
  }, []);

  function handleBackButtonClick() {
    props.navigation.goBack();
    return true;
  }

  const pengumumanDummy = [
    {
      id: Math.random(),
      title: "SOP masuk segera terbit, cek segera",
      date: "16 Mei 2018, 21:53:27",
      admin: "Oleh : Admin",
      content:
        "Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque",
    },
    {
      id: Math.random(),
      title: "SOP masuk segera terbit, cek segera",
      date: "16 Mei 2018, 21:53:27",
      admin: "Oleh : Admin",
      content:
        "Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque",
    },
    {
      id: Math.random(),
      title: "SOP masuk segera terbit, cek segera",
      date: "16 Mei 2018, 21:53:27",
      admin: "Oleh : Admin",
      content:
        "Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque",
    },
    {
      id: Math.random(),
      title: "SOP masuk segera terbit, cek segera",
      date: "16 Mei 2018, 21:53:27",
      admin: "Oleh : Admin",
      content:
        "Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque",
    },
    {
      id: Math.random(),
      title: "SOP masuk segera terbit, cek segera",
      date: "16 Mei 2018, 21:53:27",
      admin: "Oleh : Admin",
      content:
        "Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque",
    },
  ];
  return (
    <SafeAreaView style={[Styles.container, styles.container]}>
      <GlobalStatusBar
        barStyle={"dark-content"}
        backgroundColor={"transparent"}
      />
      <Header title={"Daftar Pengumuman"}/>
      <FlatList
        data={pengumumanDummy}
        keyExtractor={(item, index) => item.id}
        renderItem={({ item, index }) => {
          return (
            <View style={styles.pengumumanWrapper}>
              <View style={styles.pengumumanTitle}>
                <Image
                  source={require("../../assets/png/PengumumanIcon.png")}
                  style={styles.pengumumanIcon}
                />
                <TextDefault
                  text={"Pengumuman"}
                  titleSize
                  color={Color.primary}
                  style={Styles.marginLeftDefault}
                />
              </View>
              <View style={Styles.marginTopSmall}>
                <TextDefault text={item.title} bold />
                <TextDefault text={item.date} color={Color.textGrey} />
                <TextDefault text={item.admin} color={Color.textGrey} />
              </View>
              <TextDefault
                text={item.content}
                style={Styles.marginTopDefault}
                color={Color.textGrey}
              />
            </View>
          );
        }}
        ListFooterComponent={()=>{
          return (
            <View style={[Styles.flexDirectRow, styles.footerContainer]}>
              <ButtonDefault
                withoutBg={true}
                buttonTitle={"Sebelumnya"}
                buttonStyle={styles.buttonStyle}
                textColor={Color.primary}
              />
              <TextDefault text={"1 dari 21"} color={Color.primary} />
              <ButtonDefault
                withoutBg={true}
                buttonTitle={"Selanjutnya"}
                buttonStyle={styles.buttonStyle}
                textColor={Color.primary}
              />
            </View>
          );
        }}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  pengumumanIcon: {
    width: 20,
    height: 20,
    tintColor: Color.primary,
  },
  container: {
    backgroundColor: Color.backgroundWhite,
  },
  pengumumanWrapper: {
    backgroundColor: Color.lineGrey,
    marginHorizontal: 20,
    marginTop: 20,
    padding: 20,
    borderRadius: 8,
  },
  pengumumanTitle: {
    flexDirection: "row",
    alignItems: "center",
  },
  footerContainer: {
    justifyContent: "space-between",
    marginHorizontal: 20,
    marginVertical: 20,
  },
  buttonStyle:{
    backgroundColor: Color.softLineGrey,
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderRadius: 8
  }
});