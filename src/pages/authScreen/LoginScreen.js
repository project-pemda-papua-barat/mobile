import {
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import Color from "../../configs/Color";
import Pengumuman from "./Pengumuman";
import GreyLine from "../../components/GreyLine";
import TextDefault from "../../components/TextDefault";
import TextInputDefault from "../../components/TextInputDefault";
import CheckBoxBlank from "../../assets/svg/CheckBoxBlank";
import ChecBoxFill from "../../assets/svg/ChecBoxFill";
import ButtonDefault from "../../components/ButtonDefault";
import Styles from "../../configs/Styles";
import GlobalStatusBar from "../../components/GlobalStatusBar";
import { useDispatch, useSelector } from "react-redux";
import { postUserData } from "../../redux/actions/authAction";
import Loading from "../../components/Loading";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getUserProfile } from "../../redux/actions/profileAction";

export default function LoginScreen(props) {
  const dispatch = useDispatch();
  const { error, loading } = useSelector((state) => state.authReducer);
  const [isCheck, setIsCheck] = useState(false);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errLogin, setErrLogin] = useState(error);

  useEffect(() => {
    setErrLogin(error);
  }, [error]);

  useEffect(() => {
    AsyncStorage.getItem("username", (err, res) => {
      if (res) {
        setEmail(res);
      }
    });
    setIsCheck(false)
  }, [email]);

  const signInHandler = () => {
    dispatch(postUserData(email, password)).then((response) => {
      if (response === "success") {
        isCheck && AsyncStorage.setItem("username", email);
        !isCheck && AsyncStorage.removeItem("username");
        dispatch(getUserProfile());
        props.navigation.navigate("HomeScreen");
        setErrLogin("")
        setEmail("");
        setPassword("");
      }
    })
  };

  const resetIngatSaya = () => {
    setErrLogin('')
    AsyncStorage.getItem("username", (err,res) => {
      if(res){
        AsyncStorage.removeItem("username")
        setEmail('')
      }
    })
  }

  return (
    <ImageBackground
      style={Styles.imageBgContainer}
      source={require("../../assets/png/BG3x.png")}
    >
      <SafeAreaView>
        <GlobalStatusBar
          barStyle={"dark-content"}
          backgroundColor={"transparent"}
          translucent
        />
        <Pengumuman pengumumanTitle={true} color={Color.backgroundWhite} />
        <GreyLine />
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          style={styles.container}
        >
          <ScrollView style={Styles.paddingDefault}>
            <View style={Styles.itemCenterWithMarginBottom}>
              <TextDefault
                style={Styles.marginBottomDefault}
                bold
                color={Color.primary}
                h1Size
                text={"Login"}
              />
              <TextDefault
                style={Styles.marginBottomDefault}
                text={"Masukan Email dan Password"}
              />
            </View>
            <View style={styles.loginForm}>
              <TextInputDefault
                placeholder={"Email"}
                style={Styles.marginBottomDefault}
                value={email}
                onChangeText={(val) => {
                  setEmail(val), resetIngatSaya();
                }}
              />
              <TextInputDefault
                placeholder={"Password"}
                style={Styles.marginBottomSmall}
                value={password}
                secureTextEntry
                onChangeText={(val) => {
                  setPassword(val), setErrLogin("");
                }}
              />
              {errLogin !== "" && (
                <TextDefault
                  text={error}
                  style={Styles.marginBottomDefault}
                  color={Color.error}
                />
              )}
            </View>
            <View style={[Styles.flexDirectRow, styles.ingatSayaStyle]}>
              <TouchableOpacity onPress={() => setIsCheck(!isCheck)}>
                {isCheck ? <ChecBoxFill /> : <CheckBoxBlank />}
              </TouchableOpacity>
              <TextDefault text={"Ingat saya"} />
            </View>
            <View>
              <ButtonDefault
                style={Styles.marginBottomDefault}
                buttonTitle={"Sign in"}
                onPress={signInHandler}
              />
              <View style={Styles.itemCenterWithMarginBottom}>
                <ButtonDefault
                  onPress={() => props.navigation.navigate("LupaPassword")}
                  buttonTitle={"Lupa Password"}
                  withoutBg
                  textColor={Color.primary}
                  buttonStyle={[
                    Styles.marginBottomDefault,
                    Styles.marginTopDefault,
                  ]}
                />
                <ButtonDefault
                  onPress={() =>
                    alert("DOWNLOAD MANUAL BOOK IS NOT READY YET !!")
                  }
                  buttonTitle={"Download Manual Book"}
                  withoutBg
                  textColor={Color.primary}
                  buttonStyle={Styles.marginBottomDefault}
                />
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      <Loading isVisible={loading} />
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  ingatSayaStyle: {
    width: "26%",
    justifyContent: "space-between",
    marginBottom: "20%",
  },
});
