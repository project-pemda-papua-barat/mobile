import {Dimensions, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Color from '../../configs/Color';
import TextDefault from '../../components/TextDefault';
import ViewSlider from 'react-native-view-slider';
import { useNavigation } from '@react-navigation/native';

const {width, height} = Dimensions.get('window');
export default function Pengumuman(props) {
  const navigation = useNavigation()
  const pengumumanDummy = [1, 2, 3, 4, 5];
  return (
    <ViewSlider
      renderSlides={
        <>
          {pengumumanDummy.map((item, index) => {
            return (
              <TouchableOpacity
              activeOpacity={0.95}
                onPress={() => navigation.navigate("DaftarPengumuman")}
                style={styles.viewBox}
                key={index}
              >
                <View
                  style={[
                    styles.container,
                    {
                      backgroundColor: props.backgroundColor
                        ? props.backgroundColor
                        : Color.primary,
                    },
                  ]}
                >
                    {props.pengumumanTitle === true && (
                  <View style={styles.pengumumanTitle}>
                    <Image
                      style={styles.pengumumanIcon}
                      source={require("../../assets/png/PengumumanIcon.png")}
                    />
                    <TextDefault
                      style={styles.textDefault}
                      text={"Pengumuman"}
                      bold
                      whiteColor
                    />
                  </View>
                    )}
                  <TextDefault
                    bold
                    color={props.color}
                    title
                    style={styles.pengumumanTitleInfo}
                    text={"SOP masuk segera terbit, cek segera"}
                  />
                  <TextDefault
                    color={props.color}
                    style={styles.pengumumanDate}
                    text={"16 Mei 2018, 21:53:27"}
                  />
                  <TextDefault
                    color={props.color}
                    style={styles.pengumumanDate}
                    text={"Oleh : Admin"}
                  />
                  <View style={styles.pengumumanInfo}>
                    <TextDefault
                      color={props.color}
                      text={
                        "Ac at diam tincidunt lorem non ultrices. Malesuada tortor ullamcorper velit, nibh. Sagittis scelerisque"
                      }
                    />
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
        </>
      }
      slideCount={5}
      dots={true}
      dotActiveColor={Color.primary}
      dotInactiveColor={Color.dotsGrey}
      dotsContainerStyle={styles.dotContainer}
      autoSlide={true}
      slideInterval={10000}
      key={Math.random()}
    />
  );
}

const styles = StyleSheet.create({
  viewBox: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    width: width
  },
  container: {
    padding: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Color.lineGrey
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'relative',
    justifyContent: 'center',
  },
  pengumumanTitle: {
    flexDirection: 'row',
    marginBottom: 10
  },
  pengumumanIcon: {
    marginRight: 10,
  },
  pengumumanInfo: {
    marginTop: 10,
  },
  pengumumanDate: {
    marginTop: 2,
  },
});
