import {StyleSheet, SafeAreaView, View, ImageBackground} from 'react-native';
import React, { useEffect } from 'react';
import TextDefault from '../../components/TextDefault';
import Color from '../../configs/Color';
import TextInputDefault from '../../components/TextInputDefault';
import Styles from '../../configs/Styles';
import ButtonDefault from '../../components/ButtonDefault';
import GlobalStatusBar from '../../components/GlobalStatusBar';

export default function LupaPassword(props) {  
  
  return (
    <ImageBackground
      style={Styles.imageBgContainer}
      source={require("../../assets/png/BG3x.png")}
    >
      <SafeAreaView>
        <GlobalStatusBar />
        <View style={Styles.paddingDefault}>
          <TextDefault
            text={"Lupa Password"}
            h1Size
            bold
            color={Color.primary}
          />
          <TextDefault
            style={Styles.marginTopDefault}
            text={
              "Masukan email anda dan kami akan mengirimkan link untuk melakukan perubahan password"
            }
          />
        </View>
        <View style={Styles.paddingDefault}>
          <TextInputDefault placeholder={"Masukan email"} />
        </View>
        <View style={Styles.paddingDefault}>
          <ButtonDefault
            onPress={() => alert("ON PROGRESS !!!")}
            buttonTitle={"Proses pergantian Password"}
          />
          <ButtonDefault
            withoutBg
            onPress={() => props.navigation.goBack()}
            buttonTitle={"Kembali"}
            textColor={Color.primary}
            buttonStyle={[Styles.itemCenter, Styles.marginTopDefault]}
          />
        </View>
      </SafeAreaView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({});
