import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
import Color from '../../configs/Color';

function CheckBoxBlank(props) {
  return (
    <Svg
      style={{
        width: 30,
        height: 30,
      }}
      viewBox="0 0 24 24"
      {...props}>
      <Path
        fill={Color.dotsGrey || props.fill}
        d="M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3M19,5V19H5V5H19Z"
      />
    </Svg>
  );
}

export default CheckBoxBlank;
