import * as React from "react";
import Svg, { Path } from "react-native-svg";
import Color from "../../configs/Color";

function ChevronLeft(props) {
  return (
    <Svg
      style={{
        width: 40,
        height: 40,
      }}
      viewBox="0 0 24 24"
      {...props}
    >
      <Path
        fill={Color.primary || props.fill}
        d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z"
      />
    </Svg>
  );
}

export default ChevronLeft;
