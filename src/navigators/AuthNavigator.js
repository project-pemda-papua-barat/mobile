import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from '../pages/authScreen/LoginScreen';
import LupaPassword from '../pages/authScreen/LupaPassword';
import HomeNavigator from './HomeNavigator';
import DaftarPengumuman from '../pages/authScreen/DaftarPengumuman';
import SplashScreen from '../pages/splashScreen/SplashScreen';

const Stack = createNativeStackNavigator();

export default function authNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        options={{ gestureEnabled: false }}
        name="SplashScreen"
        component={SplashScreen}
      />
      <Stack.Screen
        options={{ gestureEnabled: false }}
        name="LoginScreen"
        component={LoginScreen}
      />
      <Stack.Screen name="LupaPassword" component={LupaPassword} />
      <Stack.Screen
        options={{ gestureEnabled: false }}
        name="HomeScreen"
        component={HomeNavigator}
      />
      <Stack.Screen name="DaftarPengumuman" component={DaftarPengumuman} />
    </Stack.Navigator>
  );
}
