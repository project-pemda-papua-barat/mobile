import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import AuthNavigator from "./src/navigators/AuthNavigator";
import { Provider } from "react-redux";
import { store } from "./src/redux/store";
import { LogBox } from "react-native";
LogBox.ignoreLogs(["Remote debugger"]);

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <AuthNavigator />
      </NavigationContainer>
    </Provider>
  );
}

export default App;
